**Logout**
----
  Logs the user out.

**URL:** `/api/v1/logout`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

* **OPTIONAL:**

    : `device_id` - 64-byte Apple decive_id to remove from user's devices list

    : `platform` - `ios` or `android`

**SUCCESS:** 

* **CODE:** `200`

* **BODY:** `[]`
