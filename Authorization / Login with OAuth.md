**Login with OAuth**
----
  Signs in a user that had logged in via OAuth.

**URL:** `/api/v1/login/oauth2`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**
    
    : `id ` - Id of a user, that was returned by OAuth.

    : `token` - Access token from OAuth server

    : `platform` - `ios`, `android` or `web`

* **OPTIONAL:**

    : `device_id` - device_id,

    : `device_uuid` - device_uuid,

**SUCCESS:** 

* **CODE:** `200`

* **BODY:** 
```
#!JSON
    {
        'user': user_info, // see "Get the user details" below
        'first_login': is_first_login, // boolean
        'remember_me': remember_me_cookie, // for ios only

/// OPTIONAL

        'devices' : the_device_list // see "Add a device to the user device list" below

    }
```

[Get the user details](https://bitbucket.org/mikhail_rel/pushfor-hsbc-docs/wiki/Application%20/%20User%20profile%20/%20Get%20the%20user%20details)

[Add a device to the user device list](https://bitbucket.org/mikhail_rel/pushfor-hsbc-docs/wiki/Application%20/%20Add%20a%20device%20to%20the%20user%20device%20list)
