**Per day data gathering top 5 mime types**

----

**URL:** `/api/v1/track/mime/popular/daily`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

* **OPTIONAL:**

: `start_timestamp` - unix timestamp of interval start

: `end_timestamp` - unix timestamp of interval end

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:** `{%YY-mm-dd% : {"%mime%" : %open_times%, ... }, .. }`

**FAIL:**

* **CODE:** `401` - The client is not logged in

   : **BODY:** `{ "error": "Access denied" }`