**Get Notifications**
----
  Returns the list of notifications.

**URL:** `/api/v1/notice/get`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

* **OPTIONAL:**

    : `updated_at` - Unix timestamp to get the newer notifications

    : `order` - `asc` or `desc`
    
    : `limit` - Number of items to return
    
    : `offset` - Number of items to skip

**SUCCESS:** 

* **CODE:** `200`

* **BODY:**

```
#!JSON
[
    {
        'id' : notice_id,
        'text' : message,
        'type' : notification_type,
        'sender' : {
            'username' : sender_name,
            'avatar' : avatar_url,
            'avatar_grayscale' : grayscaled_avatar_url,
        },
        'updated' : updated_at, // timestamp

        /// OPTIONAL

        'expired_at' : expired_at, // timestamp
        'extra' : extra_data,
        'parcel_id' : parcel_id,
        'file_id' : file_id,
        'filename' : file_name,
        'folder_id' : folder_id,
        'foldername' : folder_name
    },
    ...
]
```
