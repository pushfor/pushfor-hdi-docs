**Search**
----
  Search shared files and/or shared folders in the library

**URL:** `/api/v1/library/search`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

    : `name` - Search string

* **OPTIONAL:**

    : `folder_id` - ID of folder where the search will start
    : `folder_limit` - Limit of folders in results
    : `folder_offset` - Amount of result folders to skip
    : `file_limit` - Limit of files in results
    : `file_offset` -Amount of result files to skip 

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:**
```
#!JSON
[
    {
        'id' : folder_or_file_id,
        'parent': parent_folder_id, // optional; the root folder has not this
        'name' : folder_or_file_name,
        'created' : created_at, // timestamp
        'updated' : updated_at, // timestamp
    },

    ...
]
```

Additional keys for a folder

```
    'folder' : true,
    'empty' :  true // optional
```

Additional keys for a file

```        
    'mime' : mime_type,
    'preview' : preview_url,
    'converted': boolean,
    'hash': file_hash
```
