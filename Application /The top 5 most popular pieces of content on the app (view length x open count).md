**The top 5 most popular pieces of content on the app (view length x open count)**

----

**URL:** `/api/v1/track/content/popular/get`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:** `{%mime% : %rate%, }`

**FAIL:**

* **CODE:** `401` - The client is not logged in

   : **BODY:** `{ "error": "Access denied" }`