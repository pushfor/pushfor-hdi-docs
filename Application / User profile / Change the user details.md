**Change the user details**
----
  Changes the user details.

**URL:** `/api/v1/user/change`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**
    
* **OPTIONAL:**

    : `phone` - phone number
    : `location` - location
    : `grade` - grade
    : `remove_avatar` - 1 to remove avatar
    : OR
    : `file` - Avatar file
    : OR
    : `file64` - Base64 encoded avatar file
    : OR
    : `avatar` - An avatar file URL

**SUCCESS:** 

* **CODE:** `200`

* **BODY:** see [Get the user details](https://bitbucket.org/mikhail_rel/pushfor-hsbc-docs/wiki/Application%20/%20User%20profile%20/%20Get%20the%20user%20details) for details