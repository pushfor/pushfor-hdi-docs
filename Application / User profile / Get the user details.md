**Get the user details**
----
  Returns the information about the authorized account.

**URL:** `/api/v1/user/get`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**
    
* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

* **BODY:**
```
#!JSON
    {
        'id': user_id,
        'username': name,

/// OPTIONAL FIELDS:

        'email': email,
        'phone': phone number,
        'avatar': avatar,
        'avatar_grayscale': greyscaled_avatar,
        'location' : location,
        'grade' : grade,
    }
```
