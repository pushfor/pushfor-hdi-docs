**Track app folders are clicked on**

----

**URL:** `/api/v1/track/files/stat`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

* **OPTIONAL:**

: `start_timestamp` - unix timestamp of interval start

: `end_timestamp` - unix timestamp of interval end

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:** `{'%timestamp% : 
      "user_id"       : %user_id%,
      "username"      : %username%,
      "user_location" : [%longitude%, %latitude%],
      "file_id"       : %file_id%,
      "filename"      : %filename%,
      "mime"          : %mime%,
      "time"          : %time%,
      "timestamp"     : %timestamp%,
      "type"          : %open||view%}`

**FAIL:**

* **CODE:** `401` - The client is not logged in

   : **BODY:** `{ "error": "Access denied" }`