Method accept a GPS coordinate and return a list of user IDs.

**URL:** `/api/v1/user/find/closest`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**
    
    : `coordinates` - string "{longitude float format with dot},{latitude float format with dot}"

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

**FAIL:**

* **CODE:**

    `500` - `{"error":"Invalid coordinates!"}`

* * *