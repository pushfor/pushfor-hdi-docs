**Track how long a file was opening for**

----

**URL:** `/api/v1/track/file/opening/seconds`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

: `seconds` - integer value of openening file duration in seconds

: `file_id` - openening file id

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:** `[]`

**FAIL:**

* **CODE:** `401` - The client is not logged in

   : **BODY:** `{ "error": "Access denied" }`

* **CODE:** `500` - Wrong file id or no file id provided

   : **BODY:** `{ "error": "Wrong or unknown file" }`