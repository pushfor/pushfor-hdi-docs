**Get users last geolocations**

----

**URL:** `/api/v1/track/users/locations/get`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:** `{"%user_id%":%country_full_name%}`

**FAIL:**

* **CODE:** `401` - The client is not logged in

   : **BODY:** `{ "error": "Access denied" }`