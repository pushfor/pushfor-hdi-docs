**Add a device to the user device list**
----
  Adds a device to the user device list

**URL:** `/api/v1/device/set`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

    : `device_id` - device_id,
    : `device_uuid` - device_uuid,
    : `platform` - `ios` or `android`

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

* **BODY:**
```
#!JSON
[
    {
        'devices' : the_device_list
    },
]
```