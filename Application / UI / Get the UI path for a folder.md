  Get the pathes from a folder to UI items

**URL:** `/api/v1/ui/path/folder/get`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

    : `folder_id` - ID of the folder

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:** `[]` OR
```
#!JSON
[
    {
        "tile" : tile_id,
        "type" : "folder",
        "path" : array_of_folderIds,
    },
...
]
```
