**Get the UI json**
----
  Get the json that describes UI

**URL:** `/api/v1/ui/get`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

* **OPTIONAL:**

    : `id` - the id of the client's current json

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:** the json
