  Get the pathes from a file to UI items

**URL:** `/api/v1/ui/path/file/get`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

    : `file_id` - ID of the file

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:** `[]` OR
```
#!JSON
[
    {
        "tile" : tile_id,
        "type" : "file",
    },
...
    {
        "tile" : tile_id,
        "type" : "folder",
        "path" : array_of_folderIds
    },
...
]
```
