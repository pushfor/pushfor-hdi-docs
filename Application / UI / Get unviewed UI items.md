  Get unviewed UI item ids

**URL:** `/api/v1/ui/unviewed/get`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:** 
```
#!JSON
    {
        'ui_unviewed_files' : [tile_id1, tile_id2, ...],
        'ui_unviewed_folders' : [
            {'id': tile_id, 'unviewed': count_of_unviewed_files},
            ...
        ],
    }
```
