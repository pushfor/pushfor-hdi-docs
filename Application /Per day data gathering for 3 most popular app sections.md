**Per day data gathering for 3 most popular app sections**

----

**URL:** `/api/v1/track/folder/popular/daily/get`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

* **OPTIONAL:**

: `start_timestamp` - unix timestamp of interval start

: `end_timestamp` - unix timestamp of interval end

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:** `{'%YY-mm-dd% : [{"count":%count%, "folder_id":%folder_id%, "foldername": %foldername%, ..}, ..], ..}`

**FAIL:**

* **CODE:** `401` - The client is not logged in

   : **BODY:** `{ "error": "Access denied" }`