  Captures the user's screenshot action and deletes the content.

**URL:** `/api/v1/library/file/captured`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

    : `file_id` - A file ID over which the screenshot was taken

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

* **BODY:** `[]`