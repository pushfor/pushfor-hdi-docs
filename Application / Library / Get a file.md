**Get a file**
----
  Get details about a shared file

**URL:** `/api/v1/library/file/get`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

    : `file_id` - ID of the file

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:** 
```
#!JSON
    {
        'id' : file_id,
        'name' : file_name,
        'created' : created_at, // timestamp
        'updated' : updated_at, // timestamp
        'converted': boolean,
        'parent': ID of the file's folder
        'mime': mime_type,
        'hash': file_hash,
        'preview': preview_url,
        'shared': boolean,
        'unviewed': boolean,

/// OPTIONAL FIELDS SET:

        'captured' : "the parcel or its file was screenshoted" boolean flag,
        'big_blur_gray' : thumbnail,
        'big_blur_color' : thumbnail,

/// OPTIONAL FIELDS SET:

        'path_hd': HD preview,
        'path_pdf': pdf file preview,
        'audio_path': audio file preview,
        'audio_cover_path': audio file cover preview,
        'video_path': video file preview
    }
```
