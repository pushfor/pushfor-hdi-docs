**Get a folder**
----
  Get details about a shared folder

**URL:** `/api/v1/library/folder/get`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

    : `folder_id` - ID of the folder

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:**

```
#!JSON
{
    'id' : folder_id,
    'parent': parent_folder_id
    'name' : folder_name,
    'created' : created_at, // timestamp
    'updated' : updated_at, // timestamp
    'folder' : true,
    'empty' :  true, // optional
    'unviewed_files' :  count of unviewed files, // optional
    'children': [] // array of folder's files and subfolders (see below)
}
```

see [Search](https://bitbucket.org/mikhail_rel/pushfor-hsbc-docs/wiki/Application%20/%20Search) for details about the children's item
