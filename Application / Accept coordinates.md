Method accepts User's GPS coordinates and the timestamp of the last locations.

**URL:** `/api/v1/user/coords/accept`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**
    
    : `coordinates` - string "{longitude float format with dot},{latitude float format with dot}"

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

* **BODY:** `[]`

**FAIL:**

* **CODE:**

    `500` - `{"error":"Invalid coordinates!"}`

* * *