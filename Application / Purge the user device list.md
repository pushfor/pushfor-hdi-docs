**Purge the user device list**
----
  Purges the user device list

**URL:** `/api/v1/device/purge`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

    : `platform` - `ios` or `android`

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

* **BODY:** `[ ]`

* * *
