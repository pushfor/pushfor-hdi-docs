**Popular mime types**
----
  Find a top 5 opened mime-types.

**URL:** `/api/track/get_popular_mime`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:** `{"[some_mime_type]":[times_opened][,..]}`

**FAIL:**

* **CODE:** `401` - The client is not logged in

   : **BODY:** `{ "error": "Access denied" }`