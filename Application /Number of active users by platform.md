**Number of active users by platform**

----

**URL:** `/api/v1/track/users/by/platform/get`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:** `{"android":%int%,"ios":%int%,"web":%int%}`

**FAIL:**

* **CODE:** `401` - The client is not logged in

   : **BODY:** `{ "error": "Access denied" }`