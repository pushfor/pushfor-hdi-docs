  Get details about a pushed parcel

**URL:** `/api/v1/inbox/comments/get`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

	: `parcel_id` - ID of the parcel

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:**
```
#!JSON
[
            {
                'id' : comment_id,
                'sender' : sender_info,
                'message' : text,
                'created_at' : timestamp,
            },
            ...
]
```
