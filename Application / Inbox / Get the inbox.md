**Get the inbox**
----
  Returns the list of received parcels.

**URL:** `/api/v1/inbox/get`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

* **OPTIONAL:**

    : `parcel_id` - ID of a parcel, the method returns only a parcel with the id

    : `updated_at` - Unix timestamp, the method returns only parcels that were updated intime/later than the timestamp

    : `order` - `asc` or `desc`
    
    : `limit` - Number of items to return
    
    : `offset` - Number of items to skip

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:**

```
#!JSON
[
    {
        'id' : parcel_id,
        'subject' : subject,
        'is_voice' : "the file is a voice message" boolean flag,
        'is_ticker' : is_ticker_message,
        'view_limit' : {
            'max_views' : max_views,
            'total_views' : views_count,
        },
        'time_limit' : {
            'expired_at' : timestamp, 
            'now' : timestamp,
        },
        'file': {
            'id' : file_id,
            'name' : filename,
            'strip_blur_gray' : thumbnail,
            'strip_gray' : thumbnail,
            'big_gray' : thumbnail,
            'big_blur_gray' : thumbnail,
            'big_blur_color' : thumbnail,
            'preview' : thumbnail,
            'is_deleted' : is_deleted, // optional
        },
        'sender' : {
            'username' : sender_name,
            'avatar' : avatar_url,
            'avatar_grayscale' : grayscaled_avatar_url,
        },
        'captured' : "the parcel or its file was screenshoted" boolean flag,
        'comments_count' : count of comments,
        'has_message' : "the parcel has a message" boolean flag,
        'created_at' : created_at, // timestamp
        'updated_at' : updated_at, // timestamp
        'pushed_at' : pushed_at, // timestamp
    },
    ...
]
```