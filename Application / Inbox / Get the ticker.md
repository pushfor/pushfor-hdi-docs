  Get details about a pushed parcel

**URL:** `/api/v1/inbox/ticker/get`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:** `{}` OR [Click to see](https://bitbucket.org/mikhail_rel/pushfor-hsbc-docs/wiki/Application%20/%20Inbox%20/%20View%20a%20parcel)
