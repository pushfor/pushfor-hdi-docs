  Get details about a pushed parcel

**URL:** `/api/v1/inbox/view`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

	: `parcel_id` - ID of the parcel that contains the file

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:**
```
#!JSON
    {
        'id' : parcel_id,
        'subject' : subject,
        'message' : message,
        'is_voice' : is_voice_message,
        'is_ticker' : is_ticker_message,
        'view_limit' : {
            'max_views' : max_views,
            'total_views' : views_count,
        },
        'time_limit' : {
            'expired_at' : timestamp, 
            'now' : timestamp,
        },
        'captured' : "the parcel or its file was screenshoted" boolean flag,
        'file' : {} // see the description below
        'comments': [
            {
                'id' : comment_id,
                'sender' : sender_info,
                'message' : text,
                'created_at' : timestamp,
            },
            ...
        ]
    }
```
[Click to see 'file' descriptions](https://bitbucket.org/mikhail_rel/pushfor-hsbc-docs/wiki/Application%20/%20Library%20/%20Get%20a%20file)
