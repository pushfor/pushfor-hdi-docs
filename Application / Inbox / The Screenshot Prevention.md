  Captures the user's screenshot action and deletes the content.

**URL:** `/api/v1/inbox/captured`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

    : `parcel_id` - A parcel ID over which the screenshot was taken

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

* **BODY:** `[]`