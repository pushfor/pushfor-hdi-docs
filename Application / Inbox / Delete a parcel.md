**Delete a parcel**
----
  Removes the parcel from the inbox

**URL:** `/api/v1/inbox/delete`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

    : `parcel_id` - ID of the parcel

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:** `[]`
