**Get actions statistics (open file/folder, file view)**

----

**URL:** `/api/v1/track/actions/stat`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

: `start_timestamp` - start unix timestamp

: `end_timstamp` - end unix timestamp

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:** `{%YY-mm-dd% : {'folder_open' : %times%, 'file_open' : %times%, 'file_view' : %times%},..}`

**FAIL:**

* **CODE:** `401` - The client is not logged in

   : **BODY:** `{ "error": "Access denied" }`

* **CODE:** `500` - Invalid interval

   : **BODY:** `{ "error": "Invalid interval" }`