**Track app folders are clicked on**

----

**URL:** `/api/v1/track/folder/click`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

: `folder_id` - clicked on folder id

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:** `[]`

**FAIL:**

* **CODE:** `401` - The client is not logged in

   : **BODY:** `{ "error": "Access denied" }`