**Track app folders are clicked on**

----

**URL:** `/api/v1/track/files/actions/stat`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

* **OPTIONAL:**

: `start_timestamp` - unix timestamp of interval start

: `end_timestamp` - unix timestamp of interval end

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:** `{%YY-mm-dd% : {"opening" : %duration in seconds%, "open" : %duration in seconds% }, .. }`

**FAIL:**

* **CODE:** `401` - The client is not logged in

   : **BODY:** `{ "error": "Access denied" }`