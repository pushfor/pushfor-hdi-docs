**Get the popular mime types**
----
   Returns top5 of the most popular mime types

**URL:** `/api/v1/admin/tracking/popular_mimes/get`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:** 
```
#!JSON
[
    {
       mime_type: total_views,
...
    }
]
```