**Set the UI json**
----
  Save the json

**URL:** `/api/v1/admin/ui/set`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

    : `ui` - the json

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:** `[]`
