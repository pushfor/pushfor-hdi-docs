  Download a pushed file

**URL:** `/api/v1/admin/outbox/download`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

	: `parcel_id` - ID of the parcel that contains the file

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:** the file content
