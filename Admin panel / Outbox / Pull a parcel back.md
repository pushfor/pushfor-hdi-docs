**Pull a parcel back**
----
  Removes the parcel from the inbox

**URL:** `/api/v1/admin/outbox/pull`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

    : `parcel_id` - ID of the parcel

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:** `[]`
