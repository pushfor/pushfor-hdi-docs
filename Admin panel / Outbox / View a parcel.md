  Get details about a pushed parcel

**URL:** `/api/v1/admin/outbox/view`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

	: `parcel_id` - ID of the parcel that contains the file

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:**
```
#!JSON
    {
        'id' : parcel_id,
        'subject' : subject,
        'message' : message,
        'is_voice' : is_voice_message,
        'is_ticker' : is_ticker_message,
        'total_views' : total views in the app,
        'view_limit' : {
            'max_views' : max_views,
        },
        'time_limit' : {
            'expired_at' : timestamp, 
            'now' : timestamp,
        },
        'file' : {} // see the description below
        'is_active': is_active_flag, // boolean
        'comments': [
            {
                'id' : comment_id,
                'sender' : sender_info,
                'message' : text,
                'created_at' : timestamp,
            },
            ...
        ]
    }
```
[Click to see 'file' descriptions](https://bitbucket.org/mikhail_rel/pushfor-hsbc-docs/wiki/Admin%20panel%20/%20Library%20/%20Get%20a%20file)
