**Delete a parcel**
----
  Removes the parcel from the outbox

**URL:** `/api/v1/admin/outbox/delete`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

    : `parcel_id` - ID of the parcel

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:** `[]`
