  Pushes a librarian file or a message to the inbox

**URL:** `/api/v1/admin/outbox/push`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

* **OPTIONAL:**

    : `subject` - title of the pushing

    : `message` - message text

    : `is_voice` - "the file is a voice message" boolean flag

    : `is_ticker` - "it's a ticker" boolean flag

    : `viewXTimes` - Maximum view count

    : `viewToDate` - Expiration date; unix timestamp

    : `file_id` - ID of the librarian file OR
    : `file` - A file via HTTP post method  OR
    : `file64` - Base64 encoded file content
    : `file64name` - the file64 filename
    : `file64mime` - the file64 mime-type

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:**
```
#!JSON
    {
        'parcel_id' : parcel_id,
    }
```