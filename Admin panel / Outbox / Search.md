**Search**
----
  Search in the inbox.

**URL:** `/api/v1/admin/outbox/search`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

    : `file_id` - ID of the pushed file.

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

* **BODY:** [Click to see details](https://bitbucket.org/mikhail_rel/pushfor-hsbc-docs/wiki/Admin%20panel%20/%20Outbox%20/%20Get%20the%20outbox)