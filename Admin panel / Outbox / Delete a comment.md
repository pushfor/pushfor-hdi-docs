  Removes a comment from an existing parcel

**URL:** `/api/v1/admin/outbox/comment/delete`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

	: `parcel_id` - ID of the parcel
	: `comment_id` - ID of the comment

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:** `[]`
