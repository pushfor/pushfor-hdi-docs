  Add a comment to an existing parcel

**URL:** `/api/v1/admin/outbox/comment/add`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

	: `parcel_id` - ID of the parcel
	: `message` - text

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:**
```
#!JSON
    {
        'parcel_id' : parcel_id,
        'comment_id' : comment_id,
    }
```
