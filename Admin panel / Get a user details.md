**Get a user details**
----
  Returns the information about a non-deleted user.

**URL:** `/api/v1/admin/user/get`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

    : `user_id` - user_id

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

* **BODY:** See [Get the user details](https://bitbucket.org/mikhail_rel/pushfor-hsbc-docs/wiki/Application%20/%20Get%20the%20user%20details) for details
