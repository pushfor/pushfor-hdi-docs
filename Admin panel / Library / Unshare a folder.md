**Unshare a folder**
----
  Unshare a folder

**URL:** `/api/v1/admin/library/folder/unshare`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

    : `folder_id` - ID of the folder

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:** `[]`
