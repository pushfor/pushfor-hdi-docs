**Rename a folder**
----
  Rename a folder

**URL:** `/api/v1/admin/library/folder/rename`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

    : `folder_id` - ID of the folder
    : `name` - new name of the folder; max. 33 characters

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:** `[ 'updated' : timestamp ]`
