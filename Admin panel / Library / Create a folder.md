**Create a folder**
----
  Create a folder

**URL:** `/api/v1/admin/library/folder/create`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

    : `parent_id` - ID of a parent folder
    : `name` - name of the folder; max. 33 characters

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:**

```
#!JSON
{
    'id' : ID of the folder,
    'created' : timestamp
}
```
