**Rename a file**
----
  Rename a file

**URL:** `/api/v1/admin/library/file/rename`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

    : `file_id` - ID of the file
    : `name` - new name of the file; max. 128 characters

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:** `[ 'updated' : timestamp ]`
