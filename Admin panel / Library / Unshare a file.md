**Unshare a file**
----
  Unshare a file

**URL:** `/api/v1/admin/library/file/unshare`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

    : `file_id` - ID of the file

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:** `[]`
