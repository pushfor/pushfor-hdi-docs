**Get a folder**
----
  Get details about a folder or the root

**URL:** `/api/v1/admin/library/folder/get`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

* **OPTIONAL:**

    : `folder_id` - ID of the folder, otherwise the root will be returned

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:**

```
#!JSON
{
    'id' : folder_id,
    'parent': parent_folder_id, // optional; the root folder has not parent
    'name' : folder_name,
    'created' : created_at, // timestamp
    'updated' : updated_at, // timestamp
    'folder' : true,
    'empty' :  true, // optional
    'children': [] // array of folder's files and subfolders (see below)
}
```

see [Search](https://bitbucket.org/mikhail_rel/pushfor-hsbc-docs/wiki/Admin%20panel%20/%20Search) for details about the children's item
