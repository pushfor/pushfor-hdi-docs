**Move a file**
----
  Move a file

**URL:** `/api/v1/admin/library/file/move`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

    : `file_id` - ID of the file
    : `folder_id` - ID of new parent folder

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:** `[]`
