  Resets the "screenshot captured" status for a shared file.

**URL:** `/api/v1/admin/library/file/reshare`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

    : `file_id` - ID of the file

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:** `[]`
