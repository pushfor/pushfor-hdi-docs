**Move a folder**
----
  Move a folder

**URL:** `/api/v1/admin/library/folder/move`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

    : `folder_id` - ID of the folder
    : `parent_id` - ID of new parent folder

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:** `[]`

