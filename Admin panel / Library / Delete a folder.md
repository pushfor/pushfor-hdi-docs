**Delete a folder**
----
  Delete a folder

**URL:** `/api/v1/admin/library/folder/delete`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

    : `folder_id` - ID of the folder

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:** `[]`
