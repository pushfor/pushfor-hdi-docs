**Upload a File**
----
  Upload a file to the library

**URL:** `/api/v1/admin/library/file/add`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

    : `folder_id` - ID of the destination folder
    : `file` - A file via HTTP post method

* **OPTIONAL:**

    : `params` - JSON encoded list of parameters for the uploaded file (name, description)
    : `settings` - 
```
#!JavaScript
[
    {"name":"download", "status":true} // to enable file downloading
]

```

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:**
```
#!JSON
{
    'id' : ID of the file,
    'folder_id' : ID of the folder,
    'mime' : the file mime type,
    'name' : the file name,
    'converted' : boolean,
    'created' : timestamp
}
```

