**Get a file**
----
  Get details about a file

**URL:** `/api/v1/admin/library/file/get`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

    : `file_id` - ID of the file

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:** 
```
#!JSON
    {
        'id' : file_id,
        'name' : file_name,
        'created' : created_at, // timestamp
        'updated' : updated_at, // timestamp
        'preview' : preview_url,
        'converted': boolean,
        'parent': ID of the file's folder
        'mime': mime_type,
        'hash': file_hash,
        'preview': preview_url,
        'shared': boolean,
        'total_views' : total views in the app,

/// OPTIONAL FIELDS:

        'path_hd': HD preview,
        'path_pdf': pdf file preview,
        'audio_path': audio file preview,
        'audio_cover_path': audio file cover preview,
        'video_path': video file preview
    }
```
