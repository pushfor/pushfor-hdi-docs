**Delete**
----
  Delete files and/or folders

**URL:** `/api/v1/admin/library/delete`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

    : `ids` -

```
#!JavaScript
    {
        "files": [], // array of ids
        "folders": [] // array of ids
   }
```


* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

   : **BODY:**

```
#!JavaScript
    {
        "files": {
            "rejected": [] // array of ids
        },
        "folders": {
            "rejected": [] // array of ids
        }
   }
```
