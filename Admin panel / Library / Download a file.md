**Download a file**
----
  Download a file

**URL:** `/api/v1/admin/library/file/download`

**METHOD:** `POST`

**REQUEST PARAMS:**

* **REQUIRED:**

    : `hash` - The file hash

* **OPTIONAL:**

**SUCCESS:** 

* **CODE:** `200`

* **BODY:** the file content
